import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { BlogService } from './blog.service';
import { Post } from '../models/post.model';
import { CommonService, } from '../service/common.service';

@Component({
  selector: 'app-blog',
  templateUrl: './blog.component.html',
  styleUrls: ['./blog.component.css'],
  providers: [ BlogService ]
})
export class BlogComponent implements OnInit {
@ViewChild('closeBtn') closeBtn: ElementRef;

public posts : any [];
public post_to_delete;

constructor(private blogService: BlogService, private commonService: CommonService) {
  
}

ngOnInit(){
  this.getAllPost();

  this.commonService.postAdded_Observable.subscribe(res => {
    this.getAllPost();
  });
}

setDelete(post: Post){
  this.post_to_delete = post;
}

unsetDelete(){
  this.post_to_delete = null;
}

getAllPost(){
  this.blogService.getAllPost().subscribe(result => {
    console.log('result is ', result);
    this.posts = result['data'];
  });
}

editPost(post: Post){
  this.commonService.setPostToEdit(post);
}

deletePost(){
  this.blogService.deletePost(this.post_to_delete._id).subscribe(res => {
    this.getAllPost();
    this.closeBtn.nativeElement.click();
  })
}

}